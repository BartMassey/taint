#[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub struct Clean<T>(pub T);

impl<T> Clean<T> {
    /// Maps a `Clean<T>` to a `Clean<U>` by applying a function to the contained value.
    /// 
    /// # Examples 
    /// 
    /// Convert a `Clean<i32>` to a `Clean<String>`, consuming the original:
    /// 
    /// ```
    /// use taint::Clean;
    /// let some_clean_i32 = Clean(5);
    /// let some_clean_string = some_clean_i32.map(|i| format!("{} is a fine number", i));
    /// 
    /// assert_eq!(some_clean_string, Clean("5 is a fine number".to_string()));
    /// ```
    pub fn map<U, F>(self, f: F) -> Clean<U> where F: FnOnce(T) -> U {
        let Clean(a) = self;
        Clean(f(a))
    }
}
